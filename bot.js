class Bot extends Character {
    constructor(pos, vel, color, layers) {
        super(pos, vel, color);

        this.mutationFactor = 0.1;
        // TODO: fix the need for init with zero
        this.scores = [0];
        this.score = 0;

        this.weights = layers.slice(1).map((layer, idx) => {
            return nj.random([layers[idx], layer]).multiply(1);
        });
        this.biases = layers.slice(1).map((layer) => {
            return nj.zeros([layer]);
        });

        this.activation = layer => {
            return sigmoid(layer);
        };

        this.dead = false;
        this.control = 0;
    }

    model(X) {
        const decisionLayer = this.weights.reduce((prevMat, currMat, idx) => {
            const layerBeforeAct = prevMat.dot(currMat)
            .divide(currMat.shape[0])
            .add(this.biases[idx]);
            
            const activated = this.activation(layerBeforeAct);
            
            return activated;
        }, X);
        
        const result = decisionLayer.selection.data.indexOf(decisionLayer.max()) - 1;

        this.score += 1;
        return (result);
    }

    mutate() {
        this.weights = this.weights.map(weight => {
            return weight.add((Math.random() * 2 - 1) * this.mutationFactor);
        });
        this.biases = this.biases.map(bias => {
            return bias.add((Math.random() * 4 - 2) * this.mutationFactor);
        });
    }

    finalScore() {
        const scoresMean = this.scores.reduce((prevSum, currVal) => {
                return prevSum + currVal;
            }, 0) / this.scores.length;

        return scoresMean;
    }

    kill() {
        this.scores.push(this.score);
        this.score = 0;
        this.dead = true;
    }

    reset(screenWidth, screenHeight) {
        this.dead = false;
        this.pos.x = 300;//100 + Math.random()*(screenWidth-100);
        this.pos.y = 300;//100 + Math.random()*(screenHeight-100);
    }
}