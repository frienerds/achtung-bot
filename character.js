const TURN_RADIUS = 50;
const CHARACTER_RADIUS = 5;
const MINIMUM_TRACE_DISTANCE = 40 / TURN_RADIUS;

class Character {
    constructor(pos, vel, color, radius = CHARACTER_RADIUS, turnRadius = TURN_RADIUS) {
        this.pos = pos;
        this.vel = vel;
        this.color = color;
        this.radius = radius;
        this.turnRadius = turnRadius;
        this.trail = [];
        this.traceDistance = 0;
    }

    placeTrace() {
        if (this.traceDistance > MINIMUM_TRACE_DISTANCE) {
            const trace = { x: this.pos.x, y: this.pos.y, radius: this.radius };
            this.trail.push(trace);
            this.traceDistance = 0;

            return trace;
        }

        return undefined;
    }

    distance(point) {
        const result = ((this.pos.x - point.x) ** 2 + (this.pos.y - point.y) ** 2) ** 0.5;
        return result;
    }

    checkCollision(grid, maxRadius, screenWidth, screenHeight) {
        // Check collisions with player
        const ySign = sign(this.vel.y);
        for (let y = parseInt(this.pos.y); ySign * y <= ySign * (this.pos.y + ySign * maxRadius * 2); y += ySign) {
            const xSign = sign(this.vel.x);
            for (let x = parseInt(this.pos.x); xSign * x <= xSign * (this.pos.x + xSign * maxRadius * 2); x += xSign) {
                const key = `x${x}y${y}`;
                if (!!grid[key]) {
                    if (this.distance({ x, y }) < grid[key] + this.radius) {
                        return true;
                    }
                }
            }
        }

        // Check collisions with borders
        if (this.pos.x - this.radius < 0 || this.pos.x + this.radius > screenWidth
            || this.pos.y - this.radius < 0 || this.pos.y + this.radius > screenHeight) {
            return true;
        }

        return false;
    }

    update(control) {
        const heading = this.vel.heading();
        const velMag = this.vel.mag();
        const angularVel = velMag / this.turnRadius;

        this.vel = p5.Vector.fromAngle(heading + control * angularVel, velMag);

        this.traceDistance += this.vel.mag();
        this.pos.add(this.vel);

        return this.placeTrace();
    }

    render() {
        fill(this.color);
        stroke(this.color);
        ellipse(this.pos.x, this.pos.y, this.radius, this.radius);
    }
}