const FRAME_RATE = 30;
const INIT_VEL = 50 / FRAME_RATE;
const SCREEN_WIDTH = 1000;
const SCREEN_HEIGHT = 600;
const POOL_SCALE = 50;
const NUM_OF_PLAYERS = 1;
const NUM_OF_MATCHES = 1;
const NUM_OF_BOTS = 100;
const SURVIVORS = 20;

let updateRate = 10 * FRAME_RATE;

let bots = [];
let pause = false;
let grid;
let pressedKey;
let maxPooledGrid;

layers = [];

function initMaxPoolGrid() {
    return Array(parseInt(SCREEN_WIDTH / POOL_SCALE)
        * parseInt(SCREEN_HEIGHT / POOL_SCALE) + 4).fill(0);
}

function setup() {
    createCanvas(SCREEN_WIDTH, SCREEN_HEIGHT);
    frameRate(FRAME_RATE);
    ellipseMode(CENTER);

    grid = new Array(SCREEN_HEIGHT);

    maxPooledGrid = nj.array(initMaxPoolGrid());
    layers = [maxPooledGrid.shape[0], 40, 10, 3];

    for (let i = 0; i < NUM_OF_BOTS; i++) {
        const newBot = new Bot(createVector(0, 0),
            createVector(INIT_VEL, INIT_VEL),
            color(Math.round(Math.random() * 255)), layers);

        newBot.reset(SCREEN_WIDTH, SCREEN_HEIGHT);

        bots.push(newBot);
    }

    background(0);
}

let evolutionIter = 0;
let matchIdx = 0;
let botsBatchIdx = 1;
let matchBots = bots.slice(0, NUM_OF_PLAYERS);


function draw() {
    document.getElementById("match-idx").innerHTML = `match: ${matchIdx}`;
    document.getElementById("bots-batch-idx").innerHTML = `batch: ${botsBatchIdx}`;
    document.getElementById("evol-iter-idx").innerHTML = ` #${evolutionIter}`;

    if (pause) { return; }

    if (matchIdx === NUM_OF_MATCHES) {
        matchIdx = 0;
        botsBatchIdx++;
        matchBots = bots.slice(botsBatchIdx * NUM_OF_PLAYERS,
            botsBatchIdx * NUM_OF_PLAYERS + NUM_OF_PLAYERS);
    }

    if (!playMatch(matchBots)) {
        matchBots.forEach(bot => bot.reset(SCREEN_WIDTH, SCREEN_HEIGHT));
        matchBots = bots.slice(botsBatchIdx * NUM_OF_PLAYERS,
            botsBatchIdx * NUM_OF_PLAYERS + NUM_OF_PLAYERS);

        grid = {};
        maxPooledGrid = nj.array(initMaxPoolGrid());

        background(0);

        matchIdx += 1;
    }

    if (botsBatchIdx === NUM_OF_BOTS / NUM_OF_PLAYERS) {
        bots.sort((a, b) => {
            return b.finalScore() - a.finalScore();
        });

        console.log(bots.map(bot => bot.finalScore()));
        const best = bots.slice(0, SURVIVORS);

        bots = [...best];
        for (let botIdx = 0; botIdx < NUM_OF_BOTS / SURVIVORS - 1; botIdx++) {
            for (let bestIdx = 0; bestIdx < SURVIVORS; bestIdx++) {
                const mutatedBot = new Bot(createVector(0, 0),
                    createVector(INIT_VEL, INIT_VEL),
                    color(Math.round(Math.random() * 255)), layers);

                mutatedBot.reset(SCREEN_WIDTH, SCREEN_HEIGHT);

                mutatedBot.weights = best[bestIdx].weights.map(weight => weight.clone());
                mutatedBot.biases = best[bestIdx].biases.map(bias => bias.clone());

                mutatedBot.mutate();
                bots.push(mutatedBot);
            }
        }

        matchIdx = 0;
        botsBatchIdx = 0;
        evolutionIter += 1;
    }
}

function keyPressed() {
    pressedKey = keyCode;
}

function keyReleased() {
    pressedKey = 0;
}

function playMatch(players) {
    for (let matchIter = 0; matchIter < updateRate / FRAME_RATE; matchIter++) {
        players.forEach(bot => {
            if (bot.dead) { return; }
            const trace = bot.update(bot.control, SCREEN_WIDTH, SCREEN_HEIGHT);

            if (bot.checkCollision(grid, bot.radius, SCREEN_WIDTH, SCREEN_HEIGHT)) {
                bot.kill();
            }

            if (!!trace) {
                const roundX = floor(trace.x);
                const roundY = floor(trace.y);

                const pooledGridData = maxPooledGrid.selection.data;

                grid[`x${floor(trace.x)}y${floor(trace.y)}`] = trace.radius;

                const rightX = floor((roundX + trace.radius) / POOL_SCALE)
                    + floor(roundY / POOL_SCALE) * floor(SCREEN_WIDTH / POOL_SCALE);
                if (hasValue(pooledGridData[rightX])) { pooledGridData[rightX] = 1 };

                const leftX = floor((roundX - trace.radius) / POOL_SCALE)
                    + floor(roundY / POOL_SCALE) * floor(SCREEN_WIDTH / POOL_SCALE);
                if (hasValue(pooledGridData[leftX])) pooledGridData[leftX] = 1;

                const topY = floor(roundX / POOL_SCALE)
                    + floor((roundY - trace.radius) / POOL_SCALE) * floor(SCREEN_WIDTH / POOL_SCALE);
                if (hasValue(pooledGridData[topY])) pooledGridData[topY] = 1;

                const bottomY = floor(roundX / POOL_SCALE)
                    + floor((roundY + trace.radius) / POOL_SCALE) * floor(SCREEN_WIDTH / POOL_SCALE);
                if (hasValue(pooledGridData[bottomY])) pooledGridData[bottomY] = 1;

                pooledGridData[maxPooledGrid.size - 1] = bot.pos.x;
                pooledGridData[maxPooledGrid.size - 2] = bot.pos.y;
                pooledGridData[maxPooledGrid.size - 3] = bot.vel.x;
                pooledGridData[maxPooledGrid.size - 4] = bot.vel.y;

                players.forEach(bot => bot.control = bot.model(maxPooledGrid));
            }
        });
    }

    players.forEach(bot => bot.render());

    if (players.reduce((prevAnd, curr) => curr.dead && prevAnd, true)) {
        return false;
    }

    return true;
}