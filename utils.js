const floor = Math.floor;

function sign(num) {
    return ((num > 0) ? 1 : -1);
}

function hasValue(val) {
    return (!!val) || (val === 0);
}

function relu(layer) {
    const newData = layer.selection.data.map(val => Math.max(0, val))
    layer.selection.data = newData;
    return layer;
}

function sigmoid(layer) {
    return nj.sigmoid(layer);
}

// maxPooledGrid.forEach((val,idx) => {
//     ellipse((idx%100)*10, (idx/100)*10, 5*val, 5*val)
//     });